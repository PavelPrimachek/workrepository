//
//  VideoRecorder.m
//  SampleCocos2d
//
//  Created by mac-224 on 6/13/13.
//
//

#import "VideoRecorder.h"
#import "AWScreenshot.h"

#define FRAME_WIDTH  320
#define FRAME_HEIGHT 480
#define TIME_SCALE   60

@implementation VideoRecorder

- (void) dealloc {
    [assetWriter release];
    [assetWriterInput release];
    [assetWriterTimer release];
    [assetWriterPixelBufferAdaptor release];
    [super dealloc];
}

- (void) startScreenRecording {
    NSLog(@"start screen recording");
    
    // create the AVAssetWriter
    NSString *moviePath = [[NSSearchPathForDirectoriesInDomains
                            (NSDocumentDirectory, NSUserDomainMask, YES)
                            objectAtIndex:0] stringByAppendingString:@"/video.mov"];
    NSLog(@"%@",moviePath);
    if ([[NSFileManager defaultManager] fileExistsAtPath:moviePath]){
        [[NSFileManager defaultManager] removeItemAtPath:moviePath error:nil];
    }
    
    NSURL *movieURL = [NSURL fileURLWithPath:moviePath];
    NSError *movieError = nil;
    assetWriter = [[AVAssetWriter alloc] initWithURL:movieURL
                                            fileType: AVFileTypeQuickTimeMovie
                                               error: &movieError];
    NSDictionary *assetWriterInputSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                              AVVideoCodecH264, AVVideoCodecKey,
                                              [NSNumber numberWithInt:320], AVVideoWidthKey,
                                              [NSNumber numberWithInt:480], AVVideoHeightKey,
                                              nil];
    
    assetWriterInput = [AVAssetWriterInput assetWriterInputWithMediaType: AVMediaTypeVideo
                                                          outputSettings:assetWriterInputSettings];
    assetWriterInput.expectsMediaDataInRealTime = YES;
    [assetWriter addInput:assetWriterInput];
    
    assetWriterPixelBufferAdaptor = [[AVAssetWriterInputPixelBufferAdaptor  alloc]
                                     initWithAssetWriterInput:assetWriterInput
                                     sourcePixelBufferAttributes:nil];
    [assetWriter startWriting];
    firstFrameWallClockTime = CFAbsoluteTimeGetCurrent();
    [assetWriter startSessionAtSourceTime: CMTimeMake(0, 60)];
    
    // start writing samples to it
    [assetWriterTimer release];
    assetWriterTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                        target:self
                                                      selector:@selector (writeSample:)
                                                      userInfo:nil
                                                       repeats:YES];
}

-(void) stopScreenRecording {
    if (assetWriterTimer != nil) {
        [assetWriterTimer invalidate];
        assetWriterTimer = nil;
        
        [assetWriter finishWritingWithCompletionHandler:^ {
            NSLog (@"finished writing");
        }];
    }
}

- (UIImage*)screenshot {
    return [AWScreenshot takeAsImage];
}

-(UIImage *) createARGBImageFromRGBAImage: (UIImage*)image {
    CGSize dimensions = [image size];
    
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * dimensions.width;
    NSUInteger bitsPerComponent = 8;
    
    unsigned char *rgba = malloc(bytesPerPixel * dimensions.width * dimensions.height);
    unsigned char *argb = malloc(bytesPerPixel * dimensions.width * dimensions.height);
    
    CGColorSpaceRef colorSpace = NULL;
    CGContextRef context = NULL;
    
    colorSpace = CGColorSpaceCreateDeviceRGB();
    context = CGBitmapContextCreate(rgba, dimensions.width, dimensions.height, bitsPerComponent, bytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrderDefault); // kCGBitmapByteOrder32Big
    CGContextDrawImage(context, CGRectMake(0, 0, dimensions.width, dimensions.height), [image CGImage]);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    for (int x = 0; x < dimensions.width; x++) {
        for (int y = 0; y < dimensions.height; y++) {
            NSUInteger offset = ((dimensions.width * y) + x) * bytesPerPixel;
            argb[offset + 0] = rgba[offset + 3];
            argb[offset + 1] = rgba[offset + 0];
            argb[offset + 2] = rgba[offset + 1];
            argb[offset + 3] = rgba[offset + 2];
        }
    }
    
    colorSpace = CGColorSpaceCreateDeviceRGB();
    context = CGBitmapContextCreate(argb, dimensions.width, dimensions.height, bitsPerComponent, bytesPerRow, colorSpace, kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrderDefault); // kCGBitmapByteOrder32Big
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    image = [UIImage imageWithCGImage: imageRef];
    CGImageRelease(imageRef);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    free(rgba);
    free(argb);
    
    return image;
}

-(void) writeSample: (NSTimer*) _timer {
    if (assetWriterInput.readyForMoreMediaData) {
        CVReturn cvErr = kCVReturnSuccess;
        
        // get screenshot image!
        CGImageRef image = (CGImageRef) [[self createARGBImageFromRGBAImage:[self screenshot]] CGImage];
        
        // prepare the pixel buffer
        CVPixelBufferRef pixelBuffer = NULL;
        CFDataRef imageData= CGDataProviderCopyData(CGImageGetDataProvider(image));
        cvErr = CVPixelBufferCreateWithBytes(kCFAllocatorDefault,
                                             FRAME_WIDTH,
                                             FRAME_HEIGHT,
                                             kCVPixelFormatType_32ARGB,
                                             (void*)CFDataGetBytePtr(imageData),
                                             CGImageGetBytesPerRow(image),
                                             NULL,
                                             NULL,
                                             NULL,
                                             &pixelBuffer);
        
        // calculate the time
        CFAbsoluteTime thisFrameWallClockTime = CFAbsoluteTimeGetCurrent();
        CFTimeInterval elapsedTime = thisFrameWallClockTime - firstFrameWallClockTime;
        //NSLog (@”elapsedTime: %f”, elapsedTime);
        CMTime presentationTime =  CMTimeMake (elapsedTime * TIME_SCALE, TIME_SCALE);
        
        // write the sample
        BOOL appended = [assetWriterPixelBufferAdaptor appendPixelBuffer:pixelBuffer withPresentationTime:presentationTime];
        
        if (appended) {
            NSLog (@"appended sample at time %lf", CMTimeGetSeconds(presentationTime));
        } else {
            NSLog (@"failed to append");
            [self stopScreenRecording];
        }
    }
}


@end
