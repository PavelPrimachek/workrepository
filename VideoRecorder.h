//
//  VideoRecorder.h
//  SampleCocos2d
//
//  Created by mac-224 on 6/13/13.
//
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface VideoRecorder : NSObject {
    AVAssetWriter *assetWriter;
    AVAssetWriterInput *assetWriterInput;
    NSTimer *assetWriterTimer;
    CFTimeInterval firstFrameWallClockTime;
    AVAssetWriterInputPixelBufferAdaptor *assetWriterPixelBufferAdaptor;
}

- (void) startScreenRecording;
- (void) stopScreenRecording;

@end
