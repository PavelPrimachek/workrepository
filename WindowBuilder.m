//
//  WindowChecker.m
//  SampleFireEffect
//
//  Created by mac-224 on 6/21/13.
//
//

#import "WindowBuilder.h"

#define DELAY 0.5

@interface WindowBuilder()
- (void)moveFire:(Window *)fireWindow;
@end

@implementation WindowBuilder

@synthesize layer = _layer;

#pragma mark - init / dealloc methods

- (id)initWithWindow:(Window *)window letterPoints:(NSArray *)letterPoints {
    self = [super init];
    if (self) {
        _letterPoints = [letterPoints copy];
        
        _allWindows = [NSMutableArray array];
        [_allWindows addObject:[NSValue valueWithCGPoint:window.position]];
        
        _currentWindows = [NSMutableArray array];
        [_currentWindows addObject:window];
        
        _fires = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc {
    self.layer = nil;
    [_letterPoints release];
    [super dealloc];
}

#pragma mark - Public methods.

- (void)buildPath {
    BOOL repeat = YES; // |repeat| is thing that indicate when branchend windows wasn't found
    NSMutableArray *firesWindows = [[NSMutableArray alloc] init];
    float delay = 0.0;
    while (repeat) {
        NSMutableArray *removableWindows = [NSMutableArray array];
        NSMutableArray *addedWindows = [NSMutableArray array];
        
        for (Window *window in _currentWindows) {
            NSMutableArray *branchedWindows = [NSMutableArray array];
            // |vert| and |hor| are variables
            // that extends search to all areas
            for (float hor = -1; hor <= 1.0; ++hor) {
                for (float vert = -1; vert <= 1.0; ++vert) {
                    
                    // condition that verifies that the search has not been done in the
                    // current window and in the window that has formed current window
                    if ((vert != 0 || hor != 0) && (vert == 0 || hor == 0) && 
                        (vert != window.creatorVertical || hor != window.creatorHorizontal)) {
                        NSArray *innerPoints = [window checkPoints:_letterPoints inRegionHorizontal:hor vertical:vert];
                        BOOL isFilling = NO;
                        float tempHor = hor;
                        float tempVert = vert;
                        int square = window.width * window.height;
                        if (square  <= innerPoints.count) {
                            isFilling = YES;
                        } else {
                            if (square * 0.6 < innerPoints.count) {
                                if (tempHor == 0) {
                                    tempHor = -0.8;
                                    while (isFilling != YES && tempHor <= 0.8) {
                                        innerPoints = [window checkPoints:_letterPoints inRegionHorizontal:tempHor vertical:tempVert];
                                        if (square * 0.8 <= innerPoints.count) {
                                            isFilling = YES;
                                        }
                                        tempHor += 0.2;
                                    }
                                } else if (tempVert == 0) {
                                    tempVert = -0.8;
                                    while (isFilling != YES && tempVert <= 0.8) {
                                        innerPoints = [window checkPoints:_letterPoints inRegionHorizontal:tempHor vertical:tempVert];
                                        if (square * 0.8 <= innerPoints.count) {
                                            isFilling = YES;
                                        }
                                        tempVert += 0.2;
                                    }
                                }
                            }
                        }
                        if (isFilling) {
                            CGPoint potentialOrigin = [window moveOriginHorizontal:tempHor vertical:tempVert];
                            // check to not create the same windows
                            BOOL repeat = NO;
                            /*
                            for (Window *w in branchedWindows) {
                                if (w.creatorHorizontal * tempVert < 0) {
                                    repeat = YES;
                                }
                                if (w.creatorVertical * tempHor < 0) {
                                    repeat =  YES;
                                }
                            }*/
                            if (![_allWindows containsObject:[NSValue valueWithCGPoint:potentialOrigin]] && !repeat) {
                                [_allWindows addObject:[NSValue valueWithCGPoint:potentialOrigin]];
                                
                                Window *newWindow = [[Window alloc] initWithOrigin:potentialOrigin
                                                                             width:[window width]
                                                                            height:[window height]
                                                                       innerPoints:innerPoints];
                                
                                newWindow.creatorHorizontal = -hor;
                                newWindow.creatorVertical = -vert;
                                [branchedWindows addObject:newWindow];
                                [_layer add:newWindow];
                                
                                [newWindow release];
                            }
                        }
                    }
                }
            }
            
            if (branchedWindows.count == 0) {
                [removableWindows addObject:window];
            }
            if (branchedWindows.count >= 1) {
                Window *temp = [branchedWindows objectAtIndex:0];
                window.creatorVertical = temp.creatorVertical;
                window.creatorHorizontal = temp.creatorHorizontal;
                window.position = temp.position;
                
                CGPoint destination = ccp(window.position.x + window.width / 2,
                                          window.position.y + window.height / 2);
                [window addAction:[CCMoveTo actionWithDuration:DELAY position:destination]];
                
                if (branchedWindows.count > 1) {
                    for (int k = 1; k < branchedWindows.count; ++k) {
                        Window *branchedWindow = [branchedWindows objectAtIndex:k];
                        branchedWindow.delay = delay;
                        [addedWindows addObject:branchedWindow];
                    }
                }
            }
        }
        
        [_currentWindows removeObjectsInArray:removableWindows];
        [_currentWindows addObjectsFromArray:addedWindows];
        [firesWindows addObjectsFromArray:removableWindows];
        if (_currentWindows.count == 0) {
            repeat = NO;
        }
        delay += DELAY;
    }
    
    for (Window *fireWindow in firesWindows) {
        [self performSelector:@selector(moveFire:) withObject:fireWindow afterDelay:fireWindow.delay];
    }
    [firesWindows release];
}

- (void)moveFire:(Window *)fireWindow {
    [fireWindow runFireActionOnLayer:_layer];
}

- (BOOL)isIntersect:(CGPoint)origin {
    Window *window = [_currentWindows objectAtIndex:0];
    float width = window.width;
    float height = window.height;
    
    for (NSValue *boxedPoint in _allWindows) {
        CGPoint point = [boxedPoint CGPointValue];
       // if (point.x > )
    }
    return NO;
}

@end
