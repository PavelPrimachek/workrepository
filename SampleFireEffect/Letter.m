//
//  Letter.m
//  SampleFireEffect
//
//  Created by mac-224 on 6/20/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "Letter.h"
#import "CCDrawingPrimitives.h"

#define SCALE 4

@implementation Letter

- (id)initWithPoints:(NSArray *)points {
    self = [super init];
    if (self) {
        _points = [points copy];
    }
    return self;
}

- (void)dealloc {
    [_points release];
    _points = nil;
    [super dealloc];
}

- (void) draw {
    glColor4ub(0, 255, 0, 0);
    glLineWidth(30.0f);
    CGPoint *verts = malloc(sizeof(CGPoint) * [_points count]);
    
    for (int i = 0; i < [_points count]; i++) {
        CGPoint point = [[_points objectAtIndex:i] CGPointValue];
        verts[i] = point;
    }
    
    ccDrawPoly(verts, [_points count], YES);
    free(verts);
}

@end
