//
//  Window.m
//  SampleFireEffect
//
//  Created by mac-224 on 6/20/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "Window.h"

@implementation Window

@synthesize delay;
@synthesize creatorHorizontal;
@synthesize creatorVertical;

# pragma mark - init / dealloc methods.

- (id)initWithOrigin:(CGPoint)origin width:(float)width height:(float)height innerPoints:(NSArray *)innerPoints {
    self = [super init];
    if (self) {
        _width = width;
        _height = height;
        _innerPoints = [innerPoints copy];
        position_ = origin;
        
        fire = [CCParticleFire node];
        fire.scale = 0.2;
        fire.position = ccp(position_.x + width / 2, position_.y + height / 2);
        actionsList = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc {
    [actionsList release];
    actionsList = nil;
    [_innerPoints release];
    [super dealloc];
}

# pragma mark - Getters.

- (float)width  {
    return _width;
}

- (float)height {
    return _height;
}

# pragma mark - Check method.

// This method checks whether the points are included in the region.
- (NSArray *)checkPoints:(NSArray *)letterPoints inRegionHorizontal:(float)hor vertical:(float)vert {
    CGPoint tempOrigin = [self moveOriginHorizontal:hor vertical:vert];
    
    NSMutableArray *checkingPoints = [NSMutableArray array];
    for (int x = tempOrigin.x; x < tempOrigin.x + _width; ++x) {
        for (int y = tempOrigin.y; y < tempOrigin.y + _height; ++y) {
            Byte pixelColor[4];
            glReadPixels(x, y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, &pixelColor[0]);
            if (pixelColor[1] == 255) {
                [checkingPoints addObject:[NSValue valueWithCGPoint:ccp(x, y)]];
            }
        }
    }
    return checkingPoints;
}

- (CGPoint)moveOriginHorizontal:(float)hor vertical:(float)vert {
    CGPoint tempOrigin = position_;
    tempOrigin.x += _width * hor;
    tempOrigin.y += _height * vert;
    return tempOrigin;
}

# pragma mark - Particle system methods

- (void)addAction:(CCMoveTo *)action {
    [actionsList addObject:action];
}

- (void)runFireActionOnLayer:(CCLayer *)layer {
    if (actionsList.count != 0){
        [layer addChild:fire];
        [fire runAction:[CCSequence actionsWithArray:actionsList]];
    }
}

# pragma mark - Method for draw itself.

- (void)draw {
    
    // Draw window
    
    glColor4f(1.0, 1.0, 1.0, 1.0);
    glLineWidth(2.0f);
    CGPoint *verts = malloc(sizeof(CGPoint) * 4);
    verts[0] = ccp(position_.x + _width, position_.y);
    verts[1] = ccp(position_.x + _width, position_.y + _height);
    verts[2] = ccp(position_.x, position_.y + _height);
    verts[3] = position_;
    
    ccDrawPoly(verts, 4, YES);
    free(verts);
    /*
    // Change color of inner points
    if (changeColorFlag) {
        glColor4f(0, 0, 255, 1.0);
    } else {
        glColor4f(0, 255, 0, 1.0);
    }
    
    CGPoint *glPoints = malloc(sizeof(CGPoint) * _innerPoints.count);
    for (int i = 0; i < _innerPoints.count; ++i) {
        glPoints[i] = [[_innerPoints objectAtIndex:i] CGPointValue];
    }
    ccDrawPoints(glPoints, _innerPoints.count);
    free(glPoints);
     */
}

@end
