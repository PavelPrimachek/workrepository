//
//  Window.h
//  SampleFireEffect
//
//  Created by mac-224 on 6/20/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "HelloWorldLayer.h"

@interface Window : CCNode {
 @private
    float _width;
    float _height;
    NSArray *_innerPoints;
    
    CCParticleFire *fire;
    NSMutableArray *actionsList;
}

@property (nonatomic) float creatorHorizontal;
@property (nonatomic) float creatorVertical;
@property (nonatomic) float delay;

- (id)initWithOrigin:(CGPoint)origin width:(float)width height:(float)height innerPoints:(NSArray *)innerPoints;

// Getters
- (float)width;
- (float)height;

// This method checks path of letter.
- (NSArray *)checkPoints:(NSArray *)letterPoints inRegionHorizontal:(float)hor vertical:(float)vert;

// This method move origin and return it.
- (CGPoint)moveOriginHorizontal:(float)hor vertical:(float)vert;

- (void)addAction:(CCMoveTo *)action;
- (void)runFireActionOnLayer:(CCLayer *)layer;

@end
