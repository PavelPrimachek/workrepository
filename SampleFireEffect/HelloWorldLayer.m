//
//  HelloWorldLayer.m
//  SampleFireEffect
//
//  Created by mac-224 on 6/13/13.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//
// http://virtualmind.ru/2012/03/19/properties-in-objective-c-in-deep/
// http://www.raywenderlich.com/6063/uikit-particle-systems-in-ios-5-tutorial
// http://www.w3.org/TR/SVG/paths.html#PathDataGeneralInformation
// http://stackoverflow.com/questions/13495766/moving-multiple-sprites-in-elliptical-path-with-uniform-speed
// http://www.syntevo.com/smartgithg/download
// bitbucket.org

#import "HelloWorldLayer.h"
#import "FRCurve.h"
#import "SVGParser.h"
#import "Letter.h"
#import "Window.h"
#import "WindowBuilder.h"

#define SCALE 4

@interface HelloWorldLayer()
- (NSArray *) ellipseWithRadius:(float)radius;
- (NSArray *) ccDrawEllipse:(CGPoint)center rx:(float)rx ry:(float)ry a:(float)a segs:(int)segs flag:(BOOL)drawLineToCenter;
@end

@implementation HelloWorldLayer

@synthesize emitter;
@synthesize pathMaker;
@synthesize recorder;

+ (CCScene *) scene {
	CCScene *scene = [CCScene node];
	HelloWorldLayer *layer = [HelloWorldLayer node];
	[scene addChild: layer];
	return scene;
}

-(id) init {
    if ((self = [super init])) {
        pathMaker = [[PathMaker alloc] init];
        recorder = [[VideoRecorder alloc] init];
        
        SVGParser *svgParser = [[SVGParser alloc] initWithFileName:@"letter_E.svg"];
        points = [[svgParser pointsForDrawing] retain];
        [svgParser release];
        
        /*
        CGPoint position = [[points objectAtIndex:0] CGPointValue];
        
        emitter = [CCParticleFire node];
        emitter.position = ccp(50, 50);
        emitter.scaleX = 0.1;
        emitter.scaleY = 0.1;
        [self addChild:emitter];
        
        CCSequence *sequence = [pathMaker pathWithPoints:points];
        [emitter runAction:sequence];
        */
        
        Letter *letter = [[Letter alloc] initWithPoints:points];
        [self addChild:letter];
        [letter release];
        
        /*
        int quantity = 30;;;
        int step = points.count / quantity;
        int i = 0;
        while (i < points.count) {
            CCParticleFire *fire = [CCParticleFire node] ;
            fire.scaleX = 0.2;
            fire.scaleY = 0.2;
            CGPoint center = [[points objectAtIndex:i] CGPointValue];
            center.x *= SCALE;
            center.y *= SCALE;
            center.y = 480 - center.y;
            fire.position = center;
            [self addChild:fire];
            fire.autoRemoveOnFinish = YES;
            i += step;
        }
        */
        self.isTouchEnabled = YES;
    }
	return self;
}

- (void) dealloc {
    [points release];
    emitter = nil;
    pathMaker = nil;
    recorder = nil;
    [super dealloc];
}

- (void)add:(CCNode *)node {
    [self addChild:node];
}

- (void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [recorder stopScreenRecording];
    
    CGPoint position = [[points objectAtIndex:0] CGPointValue];
    position.x -= 10; // 40 30 20 10 // lasat 30 30
    position.y -= 5;
    Window *startWindow = [[[Window alloc] initWithOrigin:position width:30 height:30 innerPoints:nil] autorelease];
    [self addChild:startWindow];
   
    WindowBuilder *builder = [[[WindowBuilder alloc] initWithWindow:startWindow letterPoints:points] autorelease];
    builder.layer = self;
    [builder buildPath];

    /*
    UITouch *touch = [touches anyObject];
    CGPoint location = [self convertTouchToNodeSpace:touch];
    Byte pixelColor[4];
    glReadPixels(location.x, location.y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, &pixelColor[0]);
    NSLog(@"x = %f y = %f %d %d %d",location.x, location.y, pixelColor[0],pixelColor[1],pixelColor[2]);
    
    int greenCounter = 0;
    int blackCounter = 0;
    for (int x = 0; x < 320; x++) {
        for (int y = 0; y < 480; y++) {
            Byte pixelColor[4];
            //ccColor4B *buffer = malloc(sizeof(ccColor4B));
            glReadPixels(x, y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, &pixelColor[0]);
            if (pixelColor[1] == 255) {
                greenCounter++;
            } else {
                blackCounter++;
            }
        }
    }
    NSLog(@"green points = %d black points = %d", greenCounter, blackCounter);
    */
}


# pragma mark - Additional test methods for draw ellipse.
/*

- (NSArray *) ellipseWithRadius:(float)radius {
    CGPoint centerPt = ccp(160, 240);
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < 360; i += 1)
    {
        CGPoint pt = ccpAdd(centerPt, ccpMult(ccpForAngle(CC_DEGREES_TO_RADIANS(i)), radius));
        [array addObject:[NSValue valueWithCGPoint:pt]];
    }
    return array;
}

- (NSArray *) ccDrawEllipse:(CGPoint)center rx:(float)rx ry:(float)ry a:(float)a segs:(int)segs flag:(BOOL)drawLineToCenter
{
    NSMutableArray *points = [NSMutableArray array];
    int additionalSegment = 1;
    if (drawLineToCenter)
        additionalSegment++;
    
    const float coef = 2.0f * (float)M_PI / segs;
    
    float* vertices = malloc(sizeof(float) * 2 * (segs + 2));
    if(!vertices)
    {
        return nil;
    }
    memset(vertices, 0, sizeof(float) * 2 * (segs + 2));
    
    float rads, distance, angle, j, k;
    for(int i = 0; i <= segs; ++i)
    {
        rads = i * coef;
        distance = sqrt(pow(sinf(rads) * rx, 2) + pow(cosf(rads) * ry, 2));
        angle = atan2(sinf(rads) * rx, cosf(rads) * ry);
        j = distance * cosf(angle + a) + center.x;
        k = distance * sinf(angle + a) + center.y;
        
        vertices[i*2] = j * CC_CONTENT_SCALE_FACTOR();
        vertices[i*2+1] = k * CC_CONTENT_SCALE_FACTOR();
        CGPoint point = ccp(vertices[i*2], vertices[i*2+1]);
        [points addObject:[NSValue valueWithCGPoint:point]];
    }
    vertices[(segs+1)*2] = center.x * CC_CONTENT_SCALE_FACTOR();
    vertices[(segs+1)*2+1] = center.y * CC_CONTENT_SCALE_FACTOR();
    CGPoint point = ccp(vertices[(segs+1)*2], vertices[(segs+1)*2+1]);
    [points addObject:[NSValue valueWithCGPoint:point]];
    
    // Default GL states: GL_TEXTURE_2D, GL_VERTEX_ARRAY, GL_COLOR_ARRAY, GL_TEXTURE_COORD_ARRAY
    // Needed states: GL_VERTEX_ARRAY,
    // Unneeded states: GL_TEXTURE_2D, GL_TEXTURE_COORD_ARRAY, GL_COLOR_ARRAY
    glDisable(GL_TEXTURE_2D);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    
    glVertexPointer(2, GL_FLOAT, 0, vertices);
    glDrawArrays(GL_LINE_STRIP, 0, (GLsizei) segs+additionalSegment);
    
    // restore default state
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glEnable(GL_TEXTURE_2D);
    
    free(vertices);
    return (NSArray *)points;
}

/*
 emitter = [CCParticleFire node];
 emitter.scaleX = 0.2;
 emitter.scaleY = 0.2;
 emitter.gravity = ccp(0, -100);
 emitter.position = ccp(80, 10);
 [self addChild:emitter];
 
 NSMutableArray *points = [NSMutableArray array];
 [points addObject:[NSValue valueWithCGPoint:ccp(10, 10)]];
 [points addObject:[NSValue valueWithCGPoint:ccp(160, 320)]];
 [points addObject:[NSValue valueWithCGPoint:ccp(320, 240)]];
 [points addObject:[NSValue valueWithCGPoint:ccp(250, 50)]];
 [points addObject:[NSValue valueWithCGPoint:ccp(50, 50)]];
 [points addObject:[NSValue valueWithCGPoint:ccp(300, 50)]];
 [points addObject:[NSValue valueWithCGPoint:ccp(200, 200)]];
 [points addObject:[NSValue valueWithCGPoint:ccp(100, 400)]];
 [points addObject:[NSValue valueWithCGPoint:ccp(300, 200)]];
 [points addObject:[NSValue valueWithCGPoint:ccp(120, 160)]];
 
 //CCSequence *actions = [pathMaker pathWithPoints:[self ellipseWithRadius:100.0]];
 // CCSequence *actions = [pathMaker bezierPath:points];
 //[recorder startScreenRecording];
 //[emitter runAction:actions];
 
 
 FRCurve *curve = [FRCurve curveFromType:kFRCurveBezier order:kFRCurveCubic segments:64];
 [curve setPoint:ccp(10, 10) atIndex:0];
 [curve setPoint:ccp(240, 420) atIndex:1];
 [curve setPoint:ccp(300, 120) atIndex:2];
 [curve setPoint:ccp(320, 10) atIndex:3];
 [self addChild:curve];
 [curve invalidate];
 
 NSMutableArray *controlPoints = [NSMutableArray array];
 [controlPoints addObject:[NSValue valueWithCGPoint:ccp(10, 10)]];
 [controlPoints addObject:[NSValue valueWithCGPoint:ccp(240, 420)]];
 [controlPoints addObject:[NSValue valueWithCGPoint:ccp(300, 120)]];
 [controlPoints addObject:[NSValue valueWithCGPoint:ccp(320, 10)]];
 NSArray *pointsList = [pathMaker bezierCubic:controlPoints];
 CCSequence *actions = [pathMaker pathWithPoints:pointsList];
 [emitter runAction:actions];
 

 NSArray *pointsList = [self ccDrawEllipse:ccp(160,240) rx:100 ry:200 a:M_PI segs:40 flag:true];
 CCSequence *actions = [pathMaker pathWithPoints:pointsList];
 [emitter runAction:actions];
 */

@end
