//
//  Letter.h
//  SampleFireEffect
//
//  Created by mac-224 on 6/20/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Letter : CCNode {
    NSArray *_points;
}

- (id)initWithPoints:(NSArray *)points;

@end
