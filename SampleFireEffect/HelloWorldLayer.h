//
//  HelloWorldLayer.h
//  SampleFireEffect
//
//  Created by mac-224 on 6/13/13.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//

#import "cocos2d.h"
#import "VideoRecorder.h"
#import "PathMaker.h"

@interface HelloWorldLayer : CCLayer {
    NSArray *points;
}

@property (nonatomic, retain) CCParticleSystem *emitter;
@property (nonatomic, retain) PathMaker *pathMaker;
@property (nonatomic, retain) VideoRecorder *recorder;

+ (CCScene *)scene;
- (void)add:(CCNode *)node;

@end
