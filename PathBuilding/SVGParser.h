//
//  SVGParser.h
//  SampleFireEffect
//
//  Created by mac-224 on 6/18/13.
//
//

#import <Foundation/Foundation.h>
#import "PathMaker.h"

@interface SVGParser : NSObject<NSXMLParserDelegate> {
 @private
    NSArray *_pathComponents;
    NSMutableArray *_pointsForDrawing;
    PathMaker *_pathMaker;
    NSXMLParser *_xmlParser;
}

- (id) initWithFileName:(NSString *)fileName;
- (NSArray *) pointsForDrawing;

@end
