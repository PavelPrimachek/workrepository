//
//  PathMaker.m
//  SampleFireEffect
//
//  Created by mac-224 on 6/14/13.
//
//

#import "PathMaker.h"
#import "FRCurve.h"

#define SEGMENTS 64
#define SCALE 4

@interface PathMaker()
- (float) distanceBetweenPointA:(CGPoint)a pointB:(CGPoint)b;
- (NSArray *) pointsFromCurve:(FRCurve *)curve;
@end

@implementation PathMaker

# pragma mark - Methods for get points that describes Bezier curves.

- (NSArray *) bezierCubic:(NSArray *)controlPoints {
    FRCurve *cubicCurve = [FRCurve curveFromType:kFRCurveBezier order:kFRCurveCubic segments:SEGMENTS];
    [cubicCurve setPoint:[[controlPoints objectAtIndex:0] CGPointValue] atIndex:0];
    [cubicCurve setPoint:[[controlPoints objectAtIndex:1] CGPointValue] atIndex:1];
    [cubicCurve setPoint:[[controlPoints objectAtIndex:2] CGPointValue] atIndex:2];
    [cubicCurve setPoint:[[controlPoints objectAtIndex:3] CGPointValue] atIndex:3];
    
    return [self pointsFromCurve:cubicCurve];
}

- (NSArray *) bezierQuadratic:(NSArray *)controlPoints {
    FRCurve *quadraticCurve = [FRCurve curveFromType:kFRCurveBezier order:kFRCurveQuadratic segments:SEGMENTS];
    [quadraticCurve setPoint:[[controlPoints objectAtIndex:0] CGPointValue] atIndex:0];
    [quadraticCurve setPoint:[[controlPoints objectAtIndex:1] CGPointValue] atIndex:1];
    [quadraticCurve setPoint:[[controlPoints objectAtIndex:2] CGPointValue] atIndex:2];
    
    return [self pointsFromCurve:quadraticCurve];
}

# pragma mark - Methods for get sequence of move actions.

// Returns sequence of move actions that describes motion of particle.
// Cocos2d allows plot only cubic bezier curves.
// |pointsArray| is thing that represents control points for cubic bezier curve.
- (CCSequence *) bezierPath:(NSArray *)pointsArray {
    CGPoint controlPoint1 = [[pointsArray objectAtIndex:0] CGPointValue];
    CGPoint controlPoint2 = [[pointsArray objectAtIndex:1] CGPointValue];
    CGPoint endPoint = [[pointsArray objectAtIndex:2] CGPointValue];
    
    ccBezierConfig curve;
    curve.controlPoint_1 = controlPoint1;
    curve.controlPoint_2 = controlPoint2;
    curve.endPosition = endPoint;
    
    CCBezierTo *bezierMoveAction = [CCBezierTo actionWithDuration:2.0 bezier:curve];
    return [CCSequence actions:bezierMoveAction, nil];
}

// Returns sequence of move actions.
// |pointsArray| is thing that represents motion path.
- (CCSequence *) pathWithPoints:(NSArray *)pointsArray {
    CGPoint previousPoint = [[pointsArray objectAtIndex:0] CGPointValue];
    CCArray *actionsList = [CCArray array];
    for (int i = 1; i < pointsArray.count; ++i) {
        CGPoint point = [[pointsArray objectAtIndex:i] CGPointValue];
        float duration = [self distanceBetweenPointA:previousPoint pointB:point] / 40;
        CCMoveTo *move = [CCMoveTo actionWithDuration:duration
                                             position:ccp(point.x, point.y)];
        [actionsList addObject:move];
        previousPoint = point;
    }
    return [CCSequence actionsWithArray:(NSArray *)actionsList];
}

# pragma mark - Private methods.

// Returns points that describes curve.
// |curve| is thing that represents cubic or quadrartic curve.
- (NSArray *) pointsFromCurve:(FRCurve *)curve {
    NSMutableArray *pointsList = [NSMutableArray array];
    //[pointsList addObject:[NSValue valueWithCGPoint:curve.params[0]]];
    
    float step = 1.f / (SEGMENTS - 1);
    float t = 0.f;
    for (int i = 0; i < SEGMENTS; ++i) {
        [pointsList addObject:[NSValue valueWithCGPoint:ccpDeCasteljauBezierStep(t, curve.params, curve.paramCount)]];
        t += step;
    }
    return pointsList;
}

- (float) distanceBetweenPointA:(CGPoint)a pointB:(CGPoint)b {
    float distanceX = a.x - b.x;
    float distanceY = a.y - b.y;
    return sqrtf(distanceX * distanceX + distanceY * distanceY);
}

@end
