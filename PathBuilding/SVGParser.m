//
//  SVGParser.m
//  SampleFireEffect
//
//  Created by mac-224 on 6/18/13.
//
//

#import "SVGParser.h"

#define SCALE 4

@interface SVGParser()

// Method for parse attribute <path> in svg file.
- (void) parseSvgPath:(NSString *)path;

// Method for make scale and add point to |pointForDrawing|
- (CGPoint)makeScalePoint:(CGPoint)scalingPoint;

// Methods handle the command.
// Return the index of the next command. 
- (int) commandMAtIndex:(int)index;
- (int) commandLAtIndex:(int)index;
- (int) commandCAtIndex:(int)index;
- (int) commandZAtIndex:(int)index;
@end

@implementation SVGParser

# pragma mark - init/dealloc methods

- (id) initWithFileName:(NSString *)fileName {
    self = [super init];
    if (self) {
        _pathMaker = [[PathMaker alloc] init];
        
        NSString *dataPath = [[[NSBundle mainBundle] resourcePath]
                              stringByAppendingPathComponent:fileName];
        NSData *svgData = [NSData dataWithContentsOfFile:dataPath];
        _xmlParser = [[NSXMLParser alloc] initWithData:svgData];
        [_xmlParser setDelegate:self];
    }
    return self;
}

- (void) dealloc {
    [_pathMaker release];
    [_xmlParser release];
    [_pathComponents release];
    [super dealloc];
}

# pragma mark - Getter for variable _pointsForDrawing

- (NSArray *) pointsForDrawing {
    _pointsForDrawing = [NSMutableArray array];
    [_xmlParser parse];
    return _pointsForDrawing;
}

# pragma mark - Svg Path Parsing

- (void) parseSvgPath:(NSString *)path {
    NSCharacterSet *separators = [NSCharacterSet characterSetWithCharactersInString:@", "];
    _pathComponents = [[path componentsSeparatedByCharactersInSet:separators] retain];
    
    int i = 0;
    while (i < _pathComponents.count) {
        NSString *command = [[[_pathComponents objectAtIndex:i] substringToIndex:1] uppercaseString];
        if ([command isEqualToString:@"M"]) {
            i = [self commandMAtIndex:i];
        } else if ([command isEqualToString:@"L"]) {
            i = [self commandLAtIndex:i];
        } else if ([command isEqualToString:@"C"]) {
            i = [self commandCAtIndex:i];
        } else if ([command isEqualToString:@"Z"]) {
            i = [self commandZAtIndex:i];
        }
    }
}

# pragma mark - NSXMLParserDelegate method

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI
        qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:@"path"]) {
        for (NSString *key in attributeDict) {
            //NSLog(@"key: %@ value:%@", key, [attributeDict objectForKey:key]);
            [self parseSvgPath:[attributeDict objectForKey:key]];
        }
    }
}

# pragma mark - Scaling method.

- (CGPoint)makeScalePoint:(CGPoint)scalingPoint {
    scalingPoint.x *= SCALE;
    scalingPoint.y *= SCALE;
    scalingPoint.y = 480 - scalingPoint.y;
    return scalingPoint;
    //[_pointsForDrawing addObject:[NSValue valueWithCGPoint:scalingPoint]];
}

# pragma mark - Handle command. Return index of next command.

- (int) commandMAtIndex:(int)index {
    NSString *valueX = [[_pathComponents objectAtIndex:index++] substringFromIndex:1];
    NSString *valueY = [_pathComponents objectAtIndex:index++];
    CGPoint point = ccp([valueX doubleValue], [valueY doubleValue]);
    [_pointsForDrawing addObject:[NSValue valueWithCGPoint:[self makeScalePoint:point]]];
    return index;
}

- (int) commandLAtIndex:(int)index {
    NSString *valueX = [[_pathComponents objectAtIndex:index++] substringFromIndex:1];
    NSString *valueY = [_pathComponents objectAtIndex:index++];
    CGPoint point = ccp([valueX doubleValue], [valueY doubleValue]);
    [_pointsForDrawing addObject:[NSValue valueWithCGPoint:[self makeScalePoint:point]]];
    return index;
}

- (int) commandCAtIndex:(int)index {
    NSString *x1 = [[_pathComponents objectAtIndex:index++] substringFromIndex:1];
    NSString *y1 = [_pathComponents objectAtIndex:index++];
    CGPoint controlPoint1 = [self makeScalePoint:ccp([x1 doubleValue], [y1 doubleValue])];
    
    NSString *x2 = [_pathComponents objectAtIndex:index++];
    NSString *y2 = [_pathComponents objectAtIndex:index++];
    CGPoint controlPoint2 = [self makeScalePoint:ccp([x2 doubleValue], [y2 doubleValue])];
    
    NSString *x =  [_pathComponents objectAtIndex:index++];
    NSString *y =  [_pathComponents objectAtIndex:index++];
    CGPoint endPoint = [self makeScalePoint:ccp([x doubleValue], [y doubleValue])];
    
    //[self makeScalePoint:endPoint];
    
    NSArray *controlPoints = [NSArray arrayWithObjects:
                              [_pointsForDrawing objectAtIndex:_pointsForDrawing.count - 1],
                              [NSValue valueWithCGPoint:controlPoint1],
                              [NSValue valueWithCGPoint:controlPoint2],
                              [NSValue valueWithCGPoint:endPoint], nil];
    //NSArray *bezierPoints = [_pathMaker bezierCubic:controlPoints];
    //for (NSValue *value in bezierPoints) {
      //  [self makeScalePoint:[value CGPointValue]];
    //}
    [_pointsForDrawing addObjectsFromArray:[_pathMaker bezierCubic:controlPoints]];
    
    return index;
}

- (int) commandZAtIndex:(int)index {
    [_pointsForDrawing addObject:[_pointsForDrawing objectAtIndex:0]];
    return ++index;
}

@end
