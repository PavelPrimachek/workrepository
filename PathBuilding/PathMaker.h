//
//  PathMaker.h
//  SampleFireEffect
//
//  Created by mac-224 on 6/14/13.
//
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface PathMaker : NSObject

// Returns sequence of move actions.
- (CCSequence *) pathWithPoints:(NSArray *)pointsArray;
- (CCSequence *) bezierPath:(NSArray *)pointsArray;

// Returns array of points that describes a Bezier curve.
- (NSArray *) bezierCubic:(NSArray *)controlPoints;
- (NSArray *) bezierQuadratic:(NSArray *)controlPoints;

@end
