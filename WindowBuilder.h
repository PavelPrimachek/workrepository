//
//  WindowChecker.h
//  SampleFireEffect
//
//  Created by mac-224 on 6/21/13.
//
//

#import <Foundation/Foundation.h>
#import "HelloWorldLayer.h"
#import "Window.h"

@interface WindowBuilder : NSObject {
 @private
    NSMutableArray *_allWindows;
    NSMutableArray *_currentWindows;
    NSArray *_letterPoints;
    
    NSMutableArray *_fires;
}

@property (nonatomic, retain) HelloWorldLayer *layer;

- (id)initWithWindow:(Window *)window letterPoints:(NSArray *)letterPoints;

// This method builds path and finds intercept points.
- (void)buildPath;

@end
